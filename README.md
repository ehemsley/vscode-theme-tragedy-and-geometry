# Tragedy and Geometry

A dark and colorful theme for Visual Studio Code.

![Screenshot](https://gitlab.com/ehemsley/vscode-theme-tragedy-and-geometry/raw/master/screengrab.png)

## Install

1. Go to View -> Command Palette or press Ctrl+Shift+P
2. Enter "Install Extension"
3. Write "Tragedy and Geometry"
4. Select it to install
5. Go to Command Palette
6. Press Ctrl-K Ctrl-T to change theme to "Tragedy and Geometry"

## Acknowledgements

Created using: https://github.com/Tyriar/vscode-theme-generator

## License

[MIT License](https://gitlab.com/ehemsley/vscode-theme-tragedy-and-geometry/raw/master/LICENSE)