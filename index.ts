import * as path from 'path';
import { generateTheme, IColorSet } from 'vscode-theme-generator';

const themeName = 'Tragedy and Geometry';
const colors = {
  background: '#06122F',
  foreground: '#f8f8ff',
  keyword: '#00E086',
  string: '#9381ff',
  function: '#FFAA00',
  functionCall: '#FFAA00',
  comment: '#9381ff'
}

const colorSet: IColorSet = {
  base: {
    background: colors.background,
    foreground: colors.foreground,
    color1: colors.keyword,
    color2: colors.string,
    color3: colors.function,
    color4: colors.functionCall
  },
  syntax: {
    comment: colors.comment
  },
  overrides: {
    "activityBarBadge.background": colors.comment
  }
};

generateTheme(themeName, colorSet, path.join(__dirname, 'theme.json'));
